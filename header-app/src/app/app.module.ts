import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { OneComponent } from './views/one/one.component';
import { TwoComponent } from './views/two/two.component';

const appRoutes: Routes = [
  { path: 'one', component: OneComponent },
  { path: 'two', component: TwoComponent }

];


@NgModule({
  declarations: [
    AppComponent,
    OneComponent,
    TwoComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  entryComponents:[
    AppComponent
  ],
})
export class AppModule {
  constructor(private injector: Injector){}

  ngDoBootstrap(){
    const { injector } = this
    const header = createCustomElement(AppComponent,{ injector })
    customElements.define('header-module', header)
  }
}
