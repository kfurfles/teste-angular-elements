import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from './components/atoms/input/input.component';



@NgModule({
  declarations: [InputComponent],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
