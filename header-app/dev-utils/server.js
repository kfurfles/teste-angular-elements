require('dotenv').config()
const express = require('express')
const path = require('path')
const app = express();
const DIST_PATH = path.resolve(path.join(process.cwd(),'dist','header'))
const PORT = process.env.PORT || 5001

const Server = {
    instance : {
        kill: () => ''
    },
    pid: null,
    exec(cmd){
        return runCmd(cmd, true) 
    },
    start(){
        this.instance = this.exec('npm run start')
        this.log()
        this.pid = this.instance.pid
    },
    log(){
        
    },
    restart(){
        this.instance.kill()
        this.start()
    }
}

app.use(express.static(DIST_PATH));

app.get('/*', function(req,res) {
    res.sendFile(DIST_PATH+'/index.html');
});

app.get('/refresh', function(req, res){
    Server.restart()
})

const server = app.listen(PORT,() =>{
    console.log('Listen in: '+ PORT)
});
