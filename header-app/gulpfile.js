require('dotenv').config()
var exec = require('child_process').exec;
const runCmd = require('./dev-utils/command-handler')
var gulp = require('gulp'),
    // exec = require('gulp-exec'),
    browserSync = require('browser-sync');

var pid = undefined
var proc = undefined

gulp.task('browser-sync', function() {
    browserSync.init(null, {
        server: {
            baseDir: "src",
            'no-open': true,
        }
    });
});

gulp.task('angular', function(){
    runCmd(`ng serve -o --port ${process.env.DEV_PORT}`)
})

gulp.task('serve', function(){
    if(proc){
        console.log('proc is true: ',proc)
        proc.kill()
        proc = exec('npm run start',function (err, stdout, stderr) {
            console.log(stdout);
            console.log(stderr);
        })
        pid = proc.pid
    } else {
        console.log('proc is false: ',proc)
        proc = exec('npm run start',function (err, stdout, stderr) {
            console.log(stdout);
            console.log(stderr);
        })
        pid = proc.pid
    }
})

gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('default', gulp.parallel('angular'),function () {
    // series([
        // ])
    // exec()

    // var angular = process.cwd('echo %PATH%')
    // browserSync.reload({stream:true, once: true})
    gulp.watch("src/*.**", function (params) {
        console.log('params', params)
    });
});


