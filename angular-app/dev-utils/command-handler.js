const { spawn } = require('child_process')

function runCmd(cmd, log = false){
    this.instance = spawn("cmd.exe", ["/c", cmd], { cwd: process.cwd() }) 
    
    if (log) {
        this.instance.stdout.on('data', function (data) {
            console.log("progress", data.toString('utf-8'));
        });
    
        this.instance.stderr.on('data', function (data) {
            console.log("error", data.toString('utf-8'));
        });
    
        this.instance.on('exit', function (code) {
            console.log("finished");
        });
    }
    return this.instance
}

module.exports = runCmd