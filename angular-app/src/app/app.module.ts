import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OneComponent } from './views/one/one.component';
import { TwoComponent } from './views/two/two.component';
import { HeaderComponent } from './components/header/header.component';
import { APP_BASE_HREF } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { Location } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    OneComponent,
    TwoComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  entryComponents:[
    AppComponent
  ],
  providers:[
    { provide: APP_BASE_HREF, useValue: '' }
  ]
})
export class AppModule {
  constructor(private injector: Injector, private router:Router, private location:Location){}

  ngDoBootstrap(){
    const { injector } = this
    const header = createCustomElement(AppComponent,{ injector })
    customElements.define('app-angular', header)

    //on every route change tell router to navigate to defined route
    this.location.subscribe((data)=>{
      // console.log("Data subscribe", data);
      this.router.navigateByUrl(data.url);
    });

    //using this router outlet is loaded normaly on init
    this.router.navigateByUrl(this.location.path(true));


    //event subscribe to detect route change inside angular
    this.router.events.subscribe((data)=>{
      // console.log(data);
    });
  }
}
