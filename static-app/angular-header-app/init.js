var app = {
    scripts:['header-app.js','zone.min.js','adapter.js'],
    scriptsPath(){
        return this.scripts.map(s => './angular-header-app/'+s)
    },
    init(){
        this.scriptsPath().map(s => inject(s))
    }
}

app.init()