//Install express server
const express = require('express');
const path = require('path');

const app = express();

// Serve only the static files form the dist directory
app.use(express.static(__dirname));

app.get('/a-rota-que-o-gui-escolhei', function(req,res) {
    console.log('__dirname:'+__dirname);
res.sendFile(path.join(__dirname+'/index.html'));
});

// Start the app by listening on the default Heroku port
const PORT = 5000
app.listen(PORT, () =>{
    console.log(`Server listen in port: ${PORT}`)
});